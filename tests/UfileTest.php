<?php
require __DIR__."/config.php";
class UfileTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        //存储空间名
        $this->bucket = "wdbackup";
        //上传至存储空间后的文件名称(请不要和API公私钥混淆)
        $this->savefile    = "put_test.txt";
        //待上传文件的本地路径
        $this->localfile   = __DIR__."/put_test.txt";
    }

   public function testPutFile()
   { 
        //该接口适用于0-10MB小文件,更大的文件建议使用分片上传接口
        list($data, $err) = UCloud_PutFile($this->bucket, $this->savefile, $this->localfile);
        if ($err) {
            echo "error: " . $err->ErrMsg . "\n";
            echo "code: " . $err->Code . "\n";
            exit;
        }
        echo "ETag: " . $data['ETag'] . "\n";
    }
    
    public function testGet()
    {
        function curl_file_get_contents($durl){  
            $ch = curl_init();  
            curl_setopt($ch, CURLOPT_URL, $durl);  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true) ; // 获取数据返回    
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true) ; // 在启用 CURLOPT_RETURNTRANSFER 时候将获取数据返回    
            $r = curl_exec($ch);  
            curl_close($ch);  
            return $r;  
        }

        /*
        * 访问公有$this->bucket的例子
        */
        $url = UCloud_MakePublicUrl($this->bucket, $this->savefile);
        echo "download url(public): ", $url . "\n";

        /*
        * 访问私有$this->bucket的例子
        */
        $url = UCloud_MakePrivateUrl($this->bucket, $this->savefile);
        echo "download url(private): ", $url . "\n";

        /*
        * 访问包含过期时间的私有$this->bucket例子
        */
        $curtime = time();
        $curtime += 60; // 有效期60秒
        $url = UCloud_MakePrivateUrl($this->bucket, $this->savefile, $curtime);
        $content = curl_file_get_contents($url);
        echo "download file with expires: ", $url . "\n";
    }

    public function testUploadHit()
    {
        //该接口不是上传接口.如果秒传返回非200错误码,意味着该文件在服务器不存在
        //需要继续调用其他上传接口完成上传操作
        list($data, $err) = UCloud_UploadHit($this->bucket, $this->savefile, $this->localfile);
        if ($err) {
        echo "error: " . $err->ErrMsg . "\n";
        echo "code: " . $err->Code . "\n";
        exit;
        }

        echo "upload hit success\n";
    }

    public function testDelete()
    {
        list($data, $err) = UCloud_Delete($this->bucket, $this->savefile);
        if ($err) {
            echo "error: " . $err->ErrMsg . "\n";
            echo "code: " . $err->Code . "\n";
            exit;
        }

        echo "delete $this->bucket/$this->savefile success\n";
    }

/*
    public function testMultipart()
    {
        //该接口适用于web的POST表单上传,本SDK为了完整性故带上该接口demo.
        //服务端上传建议使用分片上传接口,而非POST表单
        list($data, $err) = UCloud_MultipartForm($this->bucket, $this->savefile, $file);
        if ($err) {
            echo "error: " . $err->ErrMsg . "\n";
            echo "code: " . $err->Code . "\n";
            exit;
        }
        echo "ETag: " . $data['ETag'] . "\n";

    }

    public function testMUpload()
    {
        //初始化分片上传,获取本地上传的uploadId和分片大小
        list($data, $err) = UCloud_MInit($this->bucket, $this->savefile);
        if ($err)
        {
            echo "error: " . $err->ErrMsg . "\n";
            echo "code: " . $err->Code . "\n";
            exit;
        }

        $uploadId = $data['UploadId'];
        $blkSize  = $data['BlkSize'];
        echo "UploadId: " . $uploadId . "\n";
        echo "BlkSize:  " . $blkSize . "\n";

        //数据上传
        list($etagList, $err) = UCloud_MUpload($this->bucket, $this->savefile, $file, $uploadId, $blkSize);
        if ($err) {
            echo "error: " . $err->ErrMsg . "\n";
            echo "code: " . $err->Code . "\n";
            exit;
        }

        //完成上传
        list($data, $err) = UCloud_MFinish($this->bucket, $this->savefile, $uploadId, $etagList);
        if ($err) {
            echo "error: " . $err->ErrMsg . "\n";
            echo "code: " . $err->Code . "\n";
            exit;
        }
        echo "Etag:     " . $data['ETag'] . "\n";
        echo "FileSize: " . $data['FileSize'] . "\n";

    }
*/    
}
