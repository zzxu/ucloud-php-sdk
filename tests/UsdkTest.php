<?php
require __DIR__."/config.php";
class UsdkTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        global $UCLOUD_API_URL;
        global $UCLOUD_PUBLIC_KEY;
        global $UCLOUD_PRIVATE_KEY;
        global $PROJECT_ID;
        
        $this->conn = new UcloudApiClient(
            $UCLOUD_API_URL, 
            $UCLOUD_PUBLIC_KEY,
            $UCLOUD_PRIVATE_KEY,
            $PROJECT_ID
        );
    }

    public function testSendsms()
    { 
        $phone_str = '1377777777777|137888888888';
        $send_content = 'UCLOUD 测试短信';
        
        $params['Action'] = "SendSms";
        $params["Content"] = $send_content;
        $phones = explode("|", $phone_str);
        foreach ($phones as $key => $val) {
            $params["Phone.".$key] = $val;
        }
        print_r($response = $this->conn->get("/", $params));
    }
}
