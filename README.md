## ucloud-php-sdk
封装自 https://github.com/ucloud-web/php-sdk 使用方法请看tests下面的测试用例

### 使用方法

    composer zzxu/ucloud-php-sdk

### 单元测试

根据config.sample.php生成一个config.php,填好你自己的信息，然后运行

1) ufile

       phpunit tests/UfileTest

2) ucdn

        phpunit tests/UcdnTest


-- by xunigg

EOF 